#! /bin/sh
#  entrypoint.sh -- for docker-compose run invocations
#  Copyright © 2015, 2016, 2019  Olaf Meeuwissen
#
#  SPDX-License-Identifier: GPL-3.0-or-later

branch=$(git -C /code symbolic-ref HEAD 2>/dev/null | cut -d/ -f3-)
test -n "$branch" || branch=$(git -C /code rev-parse --short HEAD)
builddir=/build/$branch/$CC

_configure="/code/configure $CONFIGURE_OPTS"

check_configure () {
    test -x /code/configure || (cd /code; ./autogen.sh)
}

test -d $builddir || mkdir -p $builddir
cd $builddir

case "$1" in
    make)
        check_configure
        if ! test -x config.status; then
            $_configure
        fi
        ;;
    configure|*/configure)
        check_configure
        shift
        exec $_configure "$@"
        ;;
    */autogen.sh|autoupdate|autoreconf|autoconf|autoheader|aclocal|automake|autopoint|libtoolize)
        cd /code
        ;;
esac

exec "$@"
